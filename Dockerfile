FROM alpine AS base

LABEL maintainer "Brian Cole <docker@brianecole.com>"

ARG LINUX_VERSION=5.2.14

RUN apk add -U wget alpine-sdk xz

RUN mkdir /opt/src /opt/build
RUN mkdir /opt/src/linux /opt/build/linux

WORKDIR /opt/src/linux
RUN wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-$LINUX_VERSION.tar.xz
RUN tar xaf linux-$LINUX_VERSION.tar.xz
WORKDIR /opt/src/linux/linux-$LINUX_VERSION
# dependencies from https://git.alpinelinux.org/aports/tree/main/linux-vanilla/APKBUILD?id=1b6fdf43053e203b8464a9959391213e5362fe28
RUN apk add perl gmp-dev elfutils-dev bash flex bison sed installkernel bc linux-headers linux-firmware-any openssl-dev
RUN make O=/opt/build/linux allnoconfig
COPY kernel_config /opt/src/linux/linux-$LINUX_VERSION/.config
#RUN time make -j8
RUN time make -j"$(nproc)"
# outputs /opt/src/linux/linux-5.2.14/vmlinux

ARG BUSYBOX_VERSION=1.30.1
RUN mkdir /opt/src/busybox /opt/build/busybox
WORKDIR /opt/src/busybox
RUN wget https://www.busybox.net/downloads/busybox-$BUSYBOX_VERSION.tar.bz2
RUN tar xaf busybox-$BUSYBOX_VERSION.tar.bz2
WORKDIR /opt/src/busybox/busybox-$BUSYBOX_VERSION
RUN make O=/opt/build/busybox defconfig
RUN sed -e 's/.*CONFIG_STATIC.*/CONFIG_STATIC=y/' -i /opt/build/busybox/.config
WORKDIR /opt/build/busybox
RUN time make -j"$(nproc)"
RUN make install
#make CONFIG_PREFIX=. install

RUN mkdir /opt/build/initramfs
WORKDIR /opt/build/initramfs
RUN mkdir -p bin sbin etc proc sys usr/bin usr/sbin
RUN cp -a /opt/build/busybox/_install/* /opt/build/initramfs/
COPY init.sh  /opt/build/initramfs/init
RUN chmod 754 /opt/build/initramfs/init
RUN find . -print0 | cpio -o -v --null --format=newc > /opt/build/initramfs.cpio

RUN mkdir -p /opt/out
ENTRYPOINT cp /opt/build/initramfs.cpio /opt/build/linux/source/arch/x86/boot/bzImage /opt/out/
