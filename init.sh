#!/bin/sh

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devtmpfs none /dev

printf "Welcome to Brian's mini-linux build\n"
printf 'System uptime: '
awk '{print $1}' /proc/uptime
echo

getty -n -l /bin/sh 0 /dev/tty2 &
getty -n -l /bin/sh 0 /dev/tty3 &

exec /bin/sh
